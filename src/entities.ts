export interface Balle {
    x:number, 
    y:number, 
    velX:number, 
    velY:number, 
    color:string, 
    size:number
}
export interface Raquette {
    x:number, 
    y:number, 
    width:number, 
    height:number  
}
export interface Brique {
     w:number,
     h:number,
     x:number,
     y:number,
     padding:number,
     offset:number
}


