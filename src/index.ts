import { Balle, Raquette, Brique } from "./entities";


const canvas:HTMLCanvasElement = <HTMLCanvasElement>document.querySelector<HTMLElement>('canvas');
const start = document.querySelector<HTMLButtonElement>('#start');
const restart = document.querySelector<HTMLButtonElement>('#restart');
const easy = document.querySelector<HTMLButtonElement>('#easy');
const medium = document.querySelector<HTMLButtonElement>('#medium');
const hard = document.querySelector<HTMLButtonElement>('#hard');
const ctx = canvas.getContext('2d');

const width = canvas.width;
const height = canvas.height;


// OBJETS - RAQUETTE, BALLE, BRIQUE//
const balle:Balle = {x:400, y:580, velX:4, velY:-4, color:'blue', size:10};
const raquette:Raquette = {x:(width/2)-50, y:height-20, width:100, height:20};
const brique:Brique = {x:25, y:25, w:90, h:25, padding:10, offset:50};



// variables Briques //
const briqueRow = 3;
const briqueCol = 7;
const briques:any = [];


//touches directionnelles //
let rightPressed = false;
let leftPressed = false;

// compteur score
let score = 0;

//compteur vies
let vies = 3;

// request de l'animation
let reqAnim:any;

// CHOIX DU NIVEAU
let easyClicked=false;
let mediumClicked=false;
let hardClicked=false;


document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false); 



if(restart){
    restart.style.display='none';
}
if(start){
    start.style.display='none';
}


/**
 * Evenement sur bouton easy
 * Si niveau Easy choisi, disparition des autres niveaux et affichage du bouton Start.
 * @param easyClicked - variable booléenne correspondant au choix du niveau easy.
 */
easy?.addEventListener('click',()=>{
    easyClicked=true;
    console.log('easy');
    if(start){
        start.style.display='block';
    }
    if(easy){
        easy.style.display='none';
    }
    if(medium){
        medium.style.display='none';
    }
    if(hard){
        hard.style.display='none';
    }
    commands();
})

/**
 * Evenement sur bouton medium
 * Si niveau Medium choisi, disparition des autres niveaux et affichage du bouton Start.
 * @param mediumClicked - variable booléenne correspondant au choix du niveau Medium.
 */
medium?.addEventListener('click',()=>{
    mediumClicked=true;
    console.log('medium');
    if(start){
        start.style.display='block';
    }
    if(easy){
        easy.style.display='none';
    }
    if(medium){
        medium.style.display='none';
    }
    if(hard){
        hard.style.display='none';
    }
    commands();
     
})
/**
 * Evenement sur bouton hard
 * Si niveau Hard choisi, disparition des autres niveaux et affichage du bouton Start.
 * @param hardClicked - variable booléenne correspondant au choix du niveau hard.
 */
hard?.addEventListener('click',()=>{
    hardClicked=true;
    console.log('hard');
    if(start){
        start.style.display='block';
    }
    if(easy){
        easy.style.display='none';
    }
    if(medium){
        medium.style.display='none';
    }
    if(hard){
        hard.style.display='none';
    }
     commands();
})


/**
 * Evênement sur bouton Start
 * Assignation de la vitesse de Balle seon le niveau choisis.
 * @param velX - vitesse déplacement axe X de Balle.
 * @param velY - vitesse déplacement axe Y de Balle.
 */
start?.addEventListener('click', ()=> {
    if(easyClicked==true){
        console.log('easy');
        loop();
        start.style.display='none';
        if(restart){
            restart.style.display='none';
        }
    }
    if(mediumClicked==true){
        console.log('medium');
        loop();
        if(balle){
            balle.velX=5;
            balle.velY=-5;
        }
        start.style.display='none';
        if(restart){
            restart.style.display='none';
        }
    }
    if(hardClicked==true){
        console.log('hard');
        loop();
        if(balle){
            balle.velX=7;
            balle.velY=-6;
        }
        start.style.display='none';
        if(restart){
            restart.style.display='none';
        }
    }

    
})

/**
 * Evenement du bouton Restart
 * Disparition de Start et Restart
 * @param reload - Rafraichissement de la page
 */
restart?.addEventListener('click', ()=> {
    if(start){
        start.style.display='none';
    }
    if(restart){
        restart.style.display='none';
    }
    window.location.reload();
})

/**
 * Fonctionpour afficher l'explication des commandes
 * @param fillText - Intègre du texte.
 */
function commands(){
    if(ctx){
        ctx.font = "22px Courier New";
        ctx.fillStyle = "Gold";
        ctx.fillText("Touches directionnelles:", 230, height-150);
        ctx.fillText("flèche gauche et droite", 230, height-100);
    }
}


/**
 * Dessin de balle
 * @param x - coordonnées de départ de balle sur axe X
 * @param y - coordonnées de départ de balle sur axe Y
 * @param size - largeur de la balle
 * @param fillStyle - définition de la couleur
 * @param beginPath - créer un nouveau chemin
 * @param arc - ajouter un arc de cercle au tracée
 * @param fill - rempli le chemi donné avec la couleur de fond en cours
 * @param closePath - fermeture du chemin
 */
function drawBall() {
    if(ctx){
        ctx.beginPath();
        ctx.fillStyle = 'Gold';
        ctx.arc(balle.x, balle.y, balle.size, 0, 2 * Math.PI);
        ctx.fill();
        ctx.closePath();
    }
  }
/**
 * Dessin de la raquette
 * @param x - coordonnées de départ de raquette sur l'axe X
 * @param y - coordonnées de départ de raquette sur l'axe Y
 * @param width - largeur de la raquette
 * @param height - hauteur de la raquette
 * @param fillStyle - définition de la couleur
 */
function drawRaquette(){
    if(ctx){
        ctx.beginPath();
        ctx.rect(raquette.x, raquette.y, raquette.width, raquette.height);
        ctx.fillStyle = 'deepPink';
        ctx.fill();
        ctx.closePath();
    }
}
/**
 * Dessin du score
 * @param font - définition de la police et sa taille
 * @param fillStyle - définition de la couleur
 * @param fillText - concaténation avec l'état du compteur du score
 */
function drawScore(){
    if(ctx){
        ctx.font = "20px Courier New";
        ctx.fillStyle = "Gold";
        ctx.fillText("Score: "+score, 8, 20);
    }
 }

/**
 * Dessin de vies
 * @param font - définition de la police
 * @param fillStyle - définition de la couleur
 * @param fillText - concaténation avec l'état du compteur des vies
 */
 function drawLives(){
    if(ctx){
        ctx.font = "20px Courier New";
        ctx.fillStyle = "Gold";
        ctx.fillText("Vies: "+vies, canvas.width-100, 20);
    }
 }

/**
 * Iterateur sur l'objet brique
 * @param briqueCol - nombre de colonnes de briques
 * @param briqueRow - nombre de rangées de briques
 * @param c- itérateur sur les colonnes
 * @param r- itérateur sur les rangées
 * @param x- position x de la brique
 * @param y- position y de la brique
 */

for (let c = 0; c < briqueCol; c++) {
  briques[c] = [];
  for (let r = 0; r < briqueRow; r++) {
    briques[c][r] = { x: 0, y: 0, status:1};
  }
}

/**
 * Dessin des briques
 * @param ctx - cadre de dessin du jeu
 * @param w - largeur d'une brique
 * @param h - hauteur d'une brique
 * @param padding - espacement padding
 * @param offset - espacement offset
 */
function drawBrique() {
        if(ctx){
            for (let c = 0; c < briqueCol; c++) {
                for (let r = 0; r < briqueRow; r++) {
                    if(briques[c][r].status==1){
                        const briqueX = c * (brique.w + brique.padding) + brique.offset;
                        const briqueY = r * (brique.h + brique.padding) + brique.offset;
                        briques[c][r].x = briqueX;
                        briques[c][r].y = briqueY;
                        ctx.beginPath();
                        ctx.rect(briqueX, briqueY, brique.w, brique.h);
                        ctx.fillStyle = "darkOrchid";
                        ctx.fill();
                        ctx.closePath(); 
                    } 
            }
        }
    }
  }

/**
 * Détection des collisions entre balle et brique
 */
function collisionDetection(){
    for(let c=0; c<briqueCol; c++) {
        for(let r=0; r<briqueRow; r++) {
          let b = briques[c][r];
          if(b.status == 1) {
            if(balle.x+5 > b.x && balle.x+5 < b.x+brique.w && balle.y-5 > b.y && balle.y-5 < b.y+brique.h) {
                balle.velY = -(balle.velY);
                score++;
                b.status = 0; 
                drawBrique();
                    if(score==briqueCol*briqueRow){
                        stopAnim(); 
                        }
                }
            }                
      }
    }
}


/**
 * Détection des collisions entre balle et limites du canvas
 * @param x - position x 
 * @param y - position y
 * @param size - largeur de la balle
 * @param width - largeur de l'objet
 * @param velX- vitesse deplacement axe x de la balle
 * @param velY- vitesse deplacement axe y de la balle
 */

function ballMotion() {
    if ((balle.x + balle.size) >= width) {  // right //
      balle.velX = -(balle.velX);    // inversion du sens de deplacement de la balle
    }
    if ((balle.x - balle.size) <= 0) { // left //
      balle.velX = -(balle.velX);
    }
    if ((balle.y - balle.size) <= 0) {  // top //
        balle.velY = -(balle.velY);
    }
    if ((balle.y + balle.size) >= height) { // bottom //
            if(balle.x>raquette.x && balle.x <raquette.x+raquette.width){  // si la balle est a l'interieur de la raquette //
                balle.velY = -(balle.velY);
            }else{ 
                if(balle.y=height){
                    vies--;
                    balle.x = 200;
                    balle.y = 200;
                }
            }
    }
    balle.x += balle.velX;
    balle.y += balle.velY;  
}



/**
 * Arret de la méthode AnimationFrame
 * @param reqAnim - requestAnimationFrame
 * @param cancelAnimationFrame - met fin a l'animation requestAnimationFrame
 */
function stopAnim(){
    cancelAnimationFrame(reqAnim);
    if(restart){
        restart.style.display='block';
    }
}



/**
 * Assigne la valeur true lorsque la touche est enfoncée
 * @param e - retourne la valeur de la clé appuyée par l'utilisateur
 * @param rightPressed - lorsque la fleche droite est enfoncée
 * @param leftPressed - lorsque la fleche gauche est enfoncée
 */
function keyDownHandler(e:any) {
    if(e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = true; 
        console.log('right'); 
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = true; 
        console.log('left'); 
    }
}
/**
 * Assigne la valeur false lorsque la touche est relachée
 * @param e - retourne la valeur de la clé appuyée par l'utilisateur
 * @param rightPressed - lorsque la fleche droite est relachée
 * @param leftPressed - lorsque la fleche gauche est relachée
 */
function keyUpHandler(e:any) {
    if(e.key == "Right" || e.key == "ArrowRight") {
        rightPressed = false;  
    }
    else if(e.key == "Left" || e.key == "ArrowLeft") {
        leftPressed = false;  
    }
}




/**
 * FONCTION GENERALE
 * @param clearRect - méthode canvas effaçant les pixels sur une surface rectangulaire
 * @param drawBrique - dessin des briques
 * @param drawBall - dessin de la balle
 * @param drawRaquette - dessin de la raquette
 * @param ballMotion - collisions balle/murs/raquette
 * @param drawScore - affichage du score
 * @param drawLives - affichage de la vie
 * @param collisionDetection - collisions balle/briques
 * 
 */

function loop() {
    if(ctx){
        ctx.clearRect(0, 0, width, height);
    }
    drawBrique();
    drawBall();
    drawRaquette();
    ballMotion();
    drawScore();
    drawLives();
    collisionDetection();
    

    // deplacement de la raquette //
    if(rightPressed) {
        raquette.x += 7;
        if (raquette.x + raquette.width > canvas.width){
            raquette.x = canvas.width - raquette.width;
        }
    }
    else if(leftPressed) {
        raquette.x -= 7;
        if (raquette.x < 0){
            raquette.x = 0;
        }
    }

    reqAnim = requestAnimationFrame(loop);
    if(vies==0 || score==briqueCol*briqueRow){
        stopAnim();
    }
}

































